#include <iostream>
#include <torch/torch.h>
#include <torch/all.h>

using namespace std;
using namespace torch;


int main()
{
    cout << "Hello World!" << endl;

    torch::Tensor tensor = torch::rand({2, 3});
    std::cout << tensor << std::endl;

    std::cout << tensor.transpose(1,0) << std::endl;


    class Net : public nn::Module
    {
      Net(int64_t N, int64_t M)
      {
        W = register_parameter("W", torch::randn({N, M}));
        b = register_parameter("b", torch::randn(M));
      }
      Tensor forward(Tensor input)
      {
        return addmm(b, input, W);
      }
      Tensor W, b;
    };



    return 0;
}
