# libtorch-example

C++ libtorch sandbox example

## Getting started

Following <https://pytorch.org/tutorials/advanced/cpp_frontend.html> tutorial at the moment.

## Docs

<https://pytorch.org/cppdocs/>
